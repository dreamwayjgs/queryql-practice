import 'reflect-metadata'
import { Field, Int, Float, ObjectType } from 'type-graphql'
import { prop as Property, Ref, Typegoose } from "typegoose"
import * as mongoose from 'mongoose';

@ObjectType({ description: "Traccar Device Info " })
export class Device extends Typegoose {
    @Field()
    readonly _id: string;

    @Field()
    @Property({ required: true })
    uniqueid: string

    @Field(type => [Stop], { nullable: true })
    @Property()
    recentStops: Stop[]
}


@ObjectType({ description: "Skmob Stops" })
export class Stop extends Typegoose {
    @Field()
    uid: string

    @Field(type => Int)
    id: number

    @Field(type => Int)
    @Property({ required: true })
    cluster: number

    @Field(type => Float)
    lat: number

    @Field(type => Float)
    lng: number

    @Field()
    datetime: string

    @Field()
    devicetime: string

    @Field(type => Float)
    altitude: number

    @Field(type => Int)
    speed: number

    @Field(type => Int)
    course: number

    @Field()
    attributes: string

    @Field(type => Float)
    accuracy: number

    @Field()
    leaving_datetime: string
}

export const DeviceModel = new Device().getModelForClass(Device, {
    existingMongoose: mongoose,
    schemaOptions: { collection: 'device' }
})