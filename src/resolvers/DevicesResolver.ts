import 'reflect-metadata'
import { Arg, Query, Resolver } from 'type-graphql'
import { DeviceData } from '../data'
import { Device, DeviceModel } from "../schemas/Device"

@Resolver(of => Device)
export default class {
    @Query(returns => [Device])
    async fetchDevices(): Promise<DeviceData[]> {
        return await DeviceModel.find({})
    }

    @Query(returns => Device, { nullable: true })
    async Device(@Arg("uniqueid") uniqueid: string): Promise<Device> {
        return (await DeviceModel.findOne({uniqueid: uniqueid}))!
    }
}