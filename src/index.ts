import { GraphQLServer } from "graphql-yoga";
import "reflect-metadata";
import { buildSchema } from "type-graphql";
import ProjectResolver from "./resolvers/ProjectResolver";
import TaskResolver from "./resolvers/TaskResolver";
import DeviceResolver from "./resolvers/DevicesResolver"
import * as mongoose from 'mongoose'
import { readFileSync } from 'fs'


interface DbConnection {
    host: string,
    port: number,
    database: string,
    user: string,
    password: string
}

async function bootstrap() {
    const MONGODB_CONNECTION_PROPS = 'db.properties'
    const dataStr = await readFileSync(MONGODB_CONNECTION_PROPS, 'utf8')
    const data = JSON.parse(dataStr)["mongodb"]
    // console.log(data, typeof (data))
    const HOST = data.host || "localhost"
    const PORT = data.port || 27017
    const DB = data.database || ""
    const USER = data.username || "admin"
    const PASSWD = data.password || "admin"
    const URI = `mongodb://${USER}:${PASSWD}@${HOST}:${PORT}/${DB}`
    mongoose.connect(URI, { useNewUrlParser: true },
        (err) => {
            if (err) console.error
            console.log('mongod connected')
        })

    const schema = await buildSchema({
        resolvers: [ProjectResolver, TaskResolver, DeviceResolver],
        emitSchemaFile: true,
    });

    const server = new GraphQLServer({
        schema,
    });

    server.start(() => console.log("Server is running on http://localhost:4000"));
    
}

bootstrap();