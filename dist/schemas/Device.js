"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const type_graphql_1 = require("type-graphql");
const typegoose_1 = require("typegoose");
const mongoose = require("mongoose");
let Device = class Device extends typegoose_1.Typegoose {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Device.prototype, "_id", void 0);
__decorate([
    type_graphql_1.Field(),
    typegoose_1.prop({ required: true }),
    __metadata("design:type", String)
], Device.prototype, "uniqueid", void 0);
__decorate([
    type_graphql_1.Field(type => [Stop], { nullable: true }),
    typegoose_1.prop(),
    __metadata("design:type", Array)
], Device.prototype, "recentStops", void 0);
Device = __decorate([
    type_graphql_1.ObjectType({ description: "Traccar Device Info " })
], Device);
exports.Device = Device;
let Stop = class Stop extends typegoose_1.Typegoose {
};
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stop.prototype, "uid", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], Stop.prototype, "id", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    typegoose_1.prop({ required: true }),
    __metadata("design:type", Number)
], Stop.prototype, "cluster", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Stop.prototype, "lat", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Stop.prototype, "lng", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stop.prototype, "datetime", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stop.prototype, "devicetime", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Stop.prototype, "altitude", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], Stop.prototype, "speed", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Int),
    __metadata("design:type", Number)
], Stop.prototype, "course", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stop.prototype, "attributes", void 0);
__decorate([
    type_graphql_1.Field(type => type_graphql_1.Float),
    __metadata("design:type", Number)
], Stop.prototype, "accuracy", void 0);
__decorate([
    type_graphql_1.Field(),
    __metadata("design:type", String)
], Stop.prototype, "leaving_datetime", void 0);
Stop = __decorate([
    type_graphql_1.ObjectType({ description: "Skmob Stops" })
], Stop);
exports.Stop = Stop;
exports.DeviceModel = new Device().getModelForClass(Device, {
    existingMongoose: mongoose,
    schemaOptions: { collection: 'device' }
});
//# sourceMappingURL=Device.js.map