"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_1 = require("apollo-server");
require("reflect-metadata");
const type_graphql_1 = require("type-graphql");
const ProjectResolver_1 = require("./resolvers/ProjectResolver");
const TaskResolver_1 = require("./resolvers/TaskResolver");
const DevicesResolver_1 = require("./resolvers/DevicesResolver");
const mongoose = require("mongoose");
const fs_1 = require("fs");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const MONGODB_CONNECTION_PROPS = 'db.properties';
        const dataStr = yield fs_1.readFileSync(MONGODB_CONNECTION_PROPS, 'utf8');
        const data = JSON.parse(dataStr)["mongodb"];
        // console.log(data, typeof (data))
        const HOST = data.host || "localhost";
        const PORT = data.port || 27017;
        const DB = data.database || "";
        const USER = data.username || "admin";
        const PASSWD = data.password || "admin";
        const URI = `mongodb://${USER}:${PASSWD}@${HOST}:${PORT}/${DB}`;
        mongoose.connect(URI, { useNewUrlParser: true }, (err) => {
            if (err)
                console.error;
            console.log('mongod connected');
        });
        const schema = yield type_graphql_1.buildSchema({
            resolvers: [ProjectResolver_1.default, TaskResolver_1.default, DevicesResolver_1.default],
            emitSchemaFile: true,
        });
        const server = new apollo_server_1.ApolloServer({
            schema,
        });
        // server.start(() => console.log("Server is running on http://localhost:4000"));
        const { url } = yield server.listen(4000);
        console.log(`Server is running, GraphQL Playground available at ${url}`);
    });
}
bootstrap();
//# sourceMappingURL=index copy.js.map