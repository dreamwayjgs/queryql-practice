"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const type_graphql_1 = require("type-graphql");
const Device_1 = require("../schemas/Device");
let default_1 = class default_1 {
    fetchDevices() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield Device_1.DeviceModel.find({});
        });
    }
    Device(uniqueid) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield Device_1.DeviceModel.findOne({ uniqueid: uniqueid }));
        });
    }
};
__decorate([
    type_graphql_1.Query(returns => [Device_1.Device]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], default_1.prototype, "fetchDevices", null);
__decorate([
    type_graphql_1.Query(returns => Device_1.Device, { nullable: true }),
    __param(0, type_graphql_1.Arg("uniqueid")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], default_1.prototype, "Device", null);
default_1 = __decorate([
    type_graphql_1.Resolver(of => Device_1.Device)
], default_1);
exports.default = default_1;
//# sourceMappingURL=DevicesResolver.js.map