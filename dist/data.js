"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.projects = [
    { id: 1, name: "Learn React Native" },
    { id: 2, name: "Workout" },
];
exports.tasks = [
    { id: 1, title: "Install Node", completed: true, project_id: 1 },
    { id: 2, title: "Install React Native CLI:", completed: false, project_id: 1 },
    { id: 3, title: "Install Xcode", completed: false, project_id: 1 },
    { id: 4, title: "Morning Jog", completed: true, project_id: 2 },
    { id: 5, title: "Visit the gym", completed: false, project_id: 2 },
];
/*
* for local test
export const devices: DeviceData[] = [
    {
        _id: "5d034b35bacfe7679d285e9a",
        uniqueid: "4332",
        recentStops: [
            {
                "uid": "4332",
                "id": 164901,
                "cluster": 2,
                "lat": 33.2467377,
                "lng": 126.58182055,
                "datetime": "2019-07-01T00:01:09.000Z",
                "devicetime": "2019-07-01T00:41:32.000Z",
                "altitude": 82.6999969482422,
                "speed": 0,
                "course": 0,
                "attributes": "{\"batteryLevel\":73.0,\"distance\":4.61,\"totalDistance\":3.025844852E7,\"motion\":false}",
                "accuracy": 16.7520008087158,
                "leaving_datetime": "2019-07-01T00:43:33.000Z"
            },
            {
                "uid": "4332",
                "id": 166308,
                "cluster": 83,
                "lat": 33.2495295,
                "lng": 126.5638168,
                "datetime": "2019-07-01T13:46:13.000Z",
                "devicetime": "2019-07-01T14:09:58.000Z",
                "altitude": 75,
                "speed": 14.2464335954819,
                "course": 353,
                "attributes": "{\"batteryLevel\":84.0,\"distance\":54.63,\"totalDistance\":3.029770036E7,\"motion\":true}",
                "accuracy": 3,
                "leaving_datetime": "2019-07-01T14:11:01.000Z"
            },
            {
                "uid": "4332",
                "id": 166406,
                "cluster": 82,
                "lat": 33.2507222,
                "lng": 126.4134456,
                "datetime": "2019-07-01T14:32:00.000Z",
                "devicetime": "2019-07-01T15:04:43.000Z",
                "altitude": 91,
                "speed": 8.64816265302658,
                "course": 303,
                "attributes": "{\"batteryLevel\":82.0,\"distance\":35.17,\"totalDistance\":3.031301288E7,\"motion\":true}",
                "accuracy": 3,
                "leaving_datetime": "2019-07-01T15:05:48.000Z"
            }
        ]
    }
]
*/ 
//# sourceMappingURL=data.js.map